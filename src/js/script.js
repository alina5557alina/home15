alert('Theory in Readme file');
// 1
const product = {
  name: 'Iphone X',
  price: 1500,
  discount: 10,
  finishPrice: function() {
    let result = this.price - ( this.price * (this.discount / 100));
    return `${this.name} price is ${result}`
  }
}
console.log(product.finishPrice());
// 2
const user = {
 name: '',
 age: 0
}

let userName = prompt('Enter name');
let userAge = prompt('Enter age');

function greeting(name, age) {
  alert(`Hello, I'm ${age} y.o`);
}
greeting(userName, userAge);

// 3
const person = {
  name: "Alina",
  date: '30.01.2002',
  friends: ["Kate"]
}

// const copiedPerson = structuredClone(person);

const otherCopyPerson = JSON.parse(JSON.stringify(person));


console.log(person);
console.log(otherCopyPerson);